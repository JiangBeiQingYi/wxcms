<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/6/20
  Time: 上午10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">


    <title>后台管理系统</title>
    <% String path = request.getContextPath(); %>
    <link rel="stylesheet" href="<%=path%>/static/plugins/layui/css/layui.css" media="all">

</head>
<body>
<div class="container">

    <form id="linkForm" class="layui-form" method="post" action="" enctype="multipart/form-data">
        <input id="site_pk" name="site_pk" type="hidden" />
        <div class="layui-form-item" style="margin: 5% auto">
            <label class="layui-form-label">站点标题</label>
            <div class="layui-input-block">
                <input id="site_name" name="site_name"  autocomplete="off" placeholder="请输入名称" class="layui-input" type="text"/>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">域名</label>
            <div class="layui-input-block">
                <input id="site_domain" name="site_domain" lay-verify="required" autocomplete="off" placeholder="请输入域名或ip" class="layui-input" type="text">
            </div>
        </div>


        <div class="layui-form-item">
            <label class="layui-form-label">静态页</label>
            <div class="layui-input-block">
                <input id="site_static" name="site_static" lay-verify="required" autocomplete="off" placeholder="请输入静态页路径" class="layui-input" type="text">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">模板</label>
            <div class="layui-input-block">
                <input id="site_template" name="site_template" lay-verify="required" autocomplete="off" placeholder="请输入模板路径" class="layui-input" type="text">
            </div>
        </div>


        <div class="layui-form-item">
            <label class="layui-form-label">录入时间</label>
            <dvi class="layui-inline">
                <input id="site_time" name="site_time" class="layui-input" type="text" placeholder="yyyy-MM-dd" >
            </dvi>
        </div>


        <div class="layui-form-item" pane="">
            <label class="layui-form-label">是否启用</label>
            <div class="layui-input-block">
                <input id="site_state0" name="site_state" value="0" title="启用" checked type="radio"/>
                <input id="site_state1" name="site_state" value="1" title="禁用" type="radio"/>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">描述</label>
            <div class="layui-input-block">
                <input id="site_remarks" name="site_remarks" lay-verify="required" autocomplete="off" class="layui-input" type="text">
            </div>
        </div>

        <div class="layui-form-item">
            <div align="center" class="layui-input-block" style="margin: 5% auto">
                <button class="layui-btn layui-btn-small" align="center" id="edit1" onclick="edit()">保存</button>
                <button class="layui-btn layui-btn-small" id="reset" align="center" type="reset">重置</button>
            </div>
        </div>

    </form>
</div>
</body>


<script src="<%=path%>/static/plugins/layui/layui.js"></script>
<script>
    layui.use(['form','laydate','upload'],function () {
        var $ = layui.jquery;
        var form = layui.form;
        var layer = layui.layer;
        var upload = layui.upload; // 获取upload模块
        var laydate = layui.laydate;

        //时间选择器
        laydate.render({
            elem: '#site_time'
        });

        $(function (){
            debugger;
            var site_pk=$("#site_pk").val();
            if(site_pk!=null && site_pk!=''){
                $.ajax({
                    type:'post',
                    url:"<%=path%>/site/list",
                    data:{site_pk:site_pk},
                    success:function (response) {
                        var json = eval(response); // 数组
                        $.each(json,function (i) {
                            $("#site_pk").val(json[i].site_pk);
                            $("#site_name").val(json[i].site_name);
                            $("#site_domain").val(json[i].site_domain);
                            $("#site_static").val(json[i].site_static);
                            $("#site_template").val(json[i].site_template);
                            $("#site_time").val(json[i].site_time);
                            $("#site_remarks").val(json[i].site_remarks);
                            $(":radio[name='site_state'][value='"+json[i].site_state+"']").prop("checked","checked");
                        })
                        form.render('radio');
                    }
                });
            }
        });

        edit=function () {
            debugger;
            var site_pk=$("#site_pk").val();
            var site_name=$("#site_name").val();
            var url="<%=path%>/site/saveOrUpdate";
            // $("#linkForm").attr("action",url).submit();
            if(site_name!=null && site_name!=''){
                $.ajax({
                    type : 'post',
                    async : false,//同步请求，执行成功后才会继续执行后面的代码
                    url : url,
                    data : {
                        site_pk:site_pk,
                        site_name : site_name,
                        site_domain : $("#site_domain").val(),
                        site_static : $("#site_static").val(),
                        site_template : $("#site_template").val(),
                        site_time : $("#site_time").val(),
                        site_remarks : $("#site_remarks").val(),
                        site_state : $("input[name='site_state']:checked").val()
                    },
                    success : function (response) {
                        parent.layer.alert(response.msg);
                        window.location.reload();
                    },
                    error : function (response) {
                        parent.layer.alert(response.msg);
                        window.location.reload();
                    }
                });
                return false;
            }

        }
    })

</script>
</html>
