<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/12/13
  Time: 下午9:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <%--<link rel="stylesheet" href="//res.layui.com/layui/dist/css/layui.css"  media="all">--%>
    <link rel="stylesheet" href="${basePath}/static/plugins/layui/css/layui.css" media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>

<%--查询条件以及操作按钮--%>
<br>
<div class="layui-form">
    <%--查询条件--%>
    <div class="layui-form-item layui-form-pane">
        <label class="layui-form-label">站点名称</label>
        <div class="layui-input-inline">
            <input type="text" name="site_name" required  lay-verify="required" placeholder="文章标题" autocomplete="off" class="layui-input" id="site_name">
        </div>
        <%--操作按钮--%>
        <button class="layui-btn"  id="search">
            <i class="layui-icon">&#xe615;</i>搜索
        </button>
<shiro:hasPermission name="site:add">
        <button class="layui-btn" id="add">
            <i class="layui-icon">&#xe608;</i>添加
        </button>
</shiro:hasPermission>
    </div>


</div>



<%--分页数据列表--%>
<div class="layui-form">
    <table id="tablelist" class="layui-table" lay-data="{height: 'full-80', cellMinWidth: 80, page: true, limit:30, url:'${basePath}/site/pages'}" lay-filter="table">
        <thead>
        <tr>
            <th lay-data="{type:'checkbox'}">ID</th>
            <th lay-data="{field:'site_state', width:80, sort: true,templet:'#switchTpl'}">状态</th>
            <%--<th lay-data="{field:'site_pk', width:100, sort: true}">ID</th>--%>
            <th lay-data="{field:'site_name', minWidth:60}">名称</th>
            <th lay-data="{field:'site_domain', width:170}">前台地址</th>
            <th lay-data="{field:'site_static', width:170}">静态页路径</th>
            <th lay-data="{field:'site_template', width:120}">模板路径</th>
            <th lay-data="{field:'site_time', sort: true, width: 120, align: 'center'}">创建时间</th>
            <th lay-data="{field:'site_remarks', sort: true, width: 100, align: 'center'}">描述</th>
            <th lay-data="{fixed: 'right', width:150, align:'center', toolbar: '#operation'}"></th>
        </tr>
        </thead>
    </table>
</div>

<script type="text/html" id="operation">
<shiro:hasPermission name="site:edit">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
</shiro:hasPermission>
<shiro:hasPermission name="site:delete">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</shiro:hasPermission>
</script>


<script src="${basePath}/static/plugins/layui/layui.js"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script type="text/html" id="switchTpl">
    <!-- 状态渲染 -->
    {{#
    if(d.site_state === '0'){
    }}
    <span style="color: #1110f5;">启用</span>
    {{#
    }
    }}
    {{#
    if(d.site_state === '1'){
    }}
    <span style="color: #f5120f;">禁用</span>
    {{#
    }
    }}
</script>
<script>
    layui.use('table', function(){
        var $ = layui.jquery;
        var table = layui.table;

        //监听表格复选框选择
        table.on('checkbox(table)', function(obj){
            console.log(obj)
        });

        //监听工具条
        table.on('tool(table)', function(obj){
            var data = obj.data;
            if(obj.event === 'del'){
                site_delete(data.site_pk);
            } else if(obj.event === 'edit'){
//                layer.alert('编辑行：<br>'+ JSON.stringify(data))
                edit(data.site_pk);
            }
        });


        $('#add').on('click', function() {
            layer.open({
                title: '添加',
                maxmin: true,
                type: 2,
                content: 'edit.jsp',
                area: ['500px', '450px'],
                success: function(layero, index) {

                },
                end : function () {
                    table.reload("tablelist");
                }
            });
        });


        //过滤查询
        $("#search").on('click',function () {
            //执行重载
            table.reload('tablelist', {
                url: '${basePath}/site/pages',

                where: {
//                    key: {
                        site_name:$("#site_name").val(),
//                    }
                  },
                  page: {
                      curr: 1 //重新从第 1 页开始
                  }

            });
          
            
        });


        //编辑操作
        edit = function (data){
            layer.open({
                title : '编辑',
                maxmin : true,
                type : 2,
                content : 'edit.jsp',
                area : ['500px','450px'],
                success : function(layero,index){
                    var body = layer.getChildFrame('body', index); //巧妙的地方在这里哦
                    body.contents().find("#site_pk").val(data);
                },
                end : function () {
                    table.reload("tablelist");
                }
            });
        }


        //删除记录
        site_delete = function (data) {
            var flag = false;
            layer.confirm("确认删除此数据吗？", {icon: 3, title: '提示'},
                function (index) {      //确认后执行的操作
                    $.ajax({
                        type: 'post',
                        url: '${basePath}/site/delete',
                        data: {site_pk: data},
                        success: function (response) {
                            if (response.state == "success") {
                                layer.close(index);
                                parent.layer.msg("删除成功");
                                table.reload("tablelist");
                            } else if (response.state == "fail") {
                                parent.layer.alert(response.msg);
                            }
                        },
                        error: function (response) {
                            layer.close(index);
                            parent.layer.alert(response.msg);
                        }
                    });
                },
                function (index) {      //取消后执行的操作
                    flag = false;
                });
        }

    });
</script>


</body>
</html>
